import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {creationLists, RequestCreate} from "./request-create.model";
import {Api} from "../common/api";
import {contentHeaders} from "../common/headers";

declare var $: any;

@Component({
    selector: 'request-create-component',
    templateUrl: 'request-create.component.html'
})
export class RequestCreateComponent {

  public model:RequestCreate;
  public lists:creationLists;
  public alertMsg = 'temp';
  public error_msg = '';
  public success = false;
  public hideModal = false;

  constructor(public router: Router, public http: HttpClient) {
      this.initialize();
  }

  initialize() {
    this.model = new RequestCreate();
    this.lists = new creationLists();

    let body = {customerProfileCode: "BEVIL01"};
    let options = {headers: contentHeaders};

    this.http.post<creationLists>(Api.API_CREATE_INIT, body, options)
      .subscribe(
        response => {
          this.lists = response;
        },
        error => {
          this.error_msg = RequestCreateComponent.name + error.message;
          console.log("Error in " + RequestCreateComponent.name);
        }
      );
  }

  createRequest(){

    let options = {headers: contentHeaders};
    this.model.customerProfileCode = "BEVIL01";
    this.model.isProactive= $('#pro-active-yes').hasClass('active');

    this.http.put(Api.API_CREATE_REQUEST, this.model, options)
      .subscribe(
        response => {
          this.handleResponse('Request successfully created');
        },
        error => {
          this.error_msg = error.message;
          console.log(this.error_msg);
        }
      );

  }

  clean(){
    this.model = new RequestCreate();
    this.hideModal=false;
    this.error_msg = '';
    this.success=false;
    this.alertMsg = '';
  }

  handleResponse(alertMsg:string){
    this.hideModal=true;
    this.success=true;
    this.alertMsg = alertMsg;
    setTimeout(()=> $('#createRequestModal').modal('hide'), 2000);
    setTimeout(()=> this.clean(), 2100);
    setTimeout(()=> location.reload(), 2100);
  }

  createRequestByCustomer() {

  }
}
