import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {NavComponent} from './nav/nav.component';
import {FooterComponent} from "./footer/footer.component";
import {RequestCreateComponent} from "./request-create/request-create.component";
import {RequestListComponent} from "./request-list/request-list.component";
import {RequestDetailsComponent} from "./request-details/request-details.component";

import {ROUTING} from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    FooterComponent,
    RequestCreateComponent,
    RequestListComponent,
    RequestDetailsComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
