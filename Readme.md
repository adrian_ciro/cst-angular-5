# KN CST (CUSTOMER SERVICE TOOL) 

A simple base project composed by 
* Angular project (FRONTEND) 
* Java project (BACKEND)


![alt text](app.png?raw=true "App")


## Versioning

|         | Versions |
|---------|----------|
| Java    | 8        |
| Angular | 5        |

## Note

Requires manual installation since tech stack is very old

