import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from "./home/home.component";
import {RequestDetailsComponent} from "./request-details/request-details.component";

const ROUTES: Routes = [
    { path: '',       component: HomeComponent },
    { path: 'home',  component: HomeComponent },
    { path: 'details/:id',  component: RequestDetailsComponent }
];

export const ROUTING = RouterModule.forRoot(ROUTES, { useHash: true });
