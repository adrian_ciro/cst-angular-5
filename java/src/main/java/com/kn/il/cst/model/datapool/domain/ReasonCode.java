package com.kn.il.cst.model.datapool.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "REASON_CODE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReasonCode {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="reason_code_id_seq")
    @SequenceGenerator(name="reason_code_id_seq", sequenceName="REASON_CODE_SEQ", allocationSize=1, initialValue=100)
    @Column(name = "REASON_CODE_ID")
    private Long id;

    @Column
    private String reasonCode;

    @Column
    private String name;

    @Column
    private String reasonCodeType;

    @Column
    private String description;

    @Column
    private String requestedBy;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "reasonCodeParent")
    private List<ReasonCode> children;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RC_PARENT_ID")
    private ReasonCode reasonCodeParent;

    @Column
    private String cstCode;

    @Column
    @Type(type="yes_no")
    private Boolean isActive;


}
