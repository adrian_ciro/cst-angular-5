package com.kn.il.cst.model.cst.repository;

import com.kn.il.cst.AbstractJpaTest;
import com.kn.il.cst.model.cst.domain.CstRequest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;


public class CstRequestRepositoryTest extends AbstractJpaTest {

    @Autowired
    CstRequestRepository cstRequestRepository;

    @Test
    public void saveAndListTheCstRequests(){

        CstRequest req = CstRequest.builder().id(1L).requestCode("req1").customerProfileId(1L).isProactive(true).requestTypeId(1L).lccId(1L).priority("HI")
                .trace(2L).isActive(true).build();
        req.setCreatedBy("me");
        req.setUpdatedBy("me");

        cstRequestRepository.save(req);

        List<CstRequest> cstRequests = cstRequestRepository.findAll();

        assertEquals(1, cstRequests.size());
        assertNotNull(cstRequests.get(0));
    }


}