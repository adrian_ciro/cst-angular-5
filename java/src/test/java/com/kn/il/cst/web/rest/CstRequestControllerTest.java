package com.kn.il.cst.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kn.il.cst.model.cst.dto.CstRequestDto;
import com.kn.il.cst.model.cst.dto.CstRequestFilterDto;
import com.kn.il.cst.service.CstRequestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CstRequestController.class)
@AutoConfigureMockMvc
public class CstRequestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CstRequestController cstRequestController;

    @MockBean
    CstRequestService requestService;

    @Test
    public void createCstRequestWithCorrectBodyAndReturnOkRequest()  throws Exception {
        CstRequestDto requestDto = CstRequestDto.builder().customerProfileCode("GBDXY55")
                .isValid(true).description("Description").priority("HI")
                .rootCauseResponsability(1L).reasonCode1(1L).requestTypeId(1L).lccId(1L)
                .build();

        MvcResult result  = mockMvc.perform(put("/api/cstRequest")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestDto)))
                .andDo(print())
                .andReturn();

        assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void createCstRequestWithWrongBodyAndReturnBadRequest()  throws Exception {
        CstRequestDto requestDto = CstRequestDto.builder().customerProfileCode("GBDXY55").build();

        MvcResult result  = mockMvc.perform(put("/api/cstRequest")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestDto)))
                .andDo(print())
                .andReturn();

        assertEquals(result.getResponse().getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void listCstRequestsAndReturnEmptyList()  throws Exception {
        CstRequestFilterDto filterDto = CstRequestFilterDto.builder().customerProfileCode("GBDXY55").build();

        MvcResult result  = mockMvc.perform(post("/api/cstRequestList")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(filterDto)))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        assertEquals(result.getResponse().getContentAsString(), "[]");
    }

}