package com.kn.il.cst.service;

import com.kn.il.cst.model.cst.domain.CstReasonCode;
import com.kn.il.cst.model.cst.domain.CstRequest;
import com.kn.il.cst.model.cst.domain.CstRequestType;
import com.kn.il.cst.model.cst.domain.CstRootCause;
import com.kn.il.cst.model.cst.dto.*;
import com.kn.il.cst.model.cst.mapper.CstRequestMapper;
import com.kn.il.cst.model.cst.repository.CstReasonCodeRepository;
import com.kn.il.cst.model.cst.repository.CstRequestRepository;
import com.kn.il.cst.model.cst.repository.CstRequestTypeRepository;
import com.kn.il.cst.model.cst.repository.CstRootCauseRepository;
import com.kn.il.cst.util.CstDateTimeFormatter;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Getter
@Setter
public class CstRequestService {

    private CstRequestRepository cstRequestRepository;
    private CstRootCauseRepository cstRootCauseRepository;
    private CstReasonCodeRepository cstReasonCodeRepository;
    private CstRequestTypeRepository cstRequestTypeRepository;
    private LogisticsControlCenterService lccService;
    private ReasonCodeService reasonCodeService;

    private static String CURRENT_USER = "Me";

    @Autowired
    public CstRequestService(CstRequestRepository cstRequestRepository,
                             CstRootCauseRepository cstRootCauseRepository,
                             CstReasonCodeRepository cstReasonCodeRepository,
                             CstRequestTypeRepository cstRequestTypeRepository,
                             LogisticsControlCenterService logisticsControlCenterService,
                             ReasonCodeService reasonCodeService) {
        this.cstRequestRepository = cstRequestRepository;
        this.cstRootCauseRepository = cstRootCauseRepository;
        this.cstReasonCodeRepository = cstReasonCodeRepository;
        this.cstRequestTypeRepository = cstRequestTypeRepository;
        this.lccService = logisticsControlCenterService;
        this.reasonCodeService = reasonCodeService;
    }

    public void createNewCstRequest(CstRequestDto requestDto){
        CstRequest request = CstRequestMapper.mapToCstRequest(requestDto);
        request.setUpdatedBy(CURRENT_USER);
        request.setCreatedBy(CURRENT_USER);
        request.setStatusId(1L);
        request.setTrace(1L);
        request.setRequestCode(getRequestType(request.getRequestTypeId())+"-"+ CstDateTimeFormatter.parseDateTime(request.getCreated()).replaceAll("-","").replaceAll(":","").trim());
        request = cstRequestRepository.save(request);

        CstRootCause cstRootCause = CstRequestMapper.mapToRootCause(requestDto);
        cstRootCause.setCstRequest(request);
        CstReasonCode cstReasonCode = CstRequestMapper.mapToCstReasonCode(requestDto);
        cstReasonCode.setCstRequest(request);

        cstRootCauseRepository.save(cstRootCause);
        cstReasonCodeRepository.save(cstReasonCode);
    }

    public List<CstRequest> getCstRequestsByExample(CstRequestFilterDto filterDto){
        Example<CstRequest> cstRequest = Example.of(CstRequestMapper.mapToCstRequest(filterDto));
        return cstRequestRepository.findAll(cstRequest);
    }

    public Optional<CstRequestDetail2Dto> getRequestDetail(CstRequestDetailDto detailDto){

        Optional<CstRequest> request = cstRequestRepository.findById(detailDto.getId());

        if(request.isPresent()){
            CstRequestDetail2Dto reqDetail = CstRequestMapper.mapToRequestDetail2Dto(request.get());
            reqDetail.setRequestType(getRequestType(request.get().getRequestTypeId()));
            reqDetail.setRequestCode(request.get().getRequestCode());
            reqDetail.setLcc(lccService.getLccShortName(request.get().getLccId()));
            reqDetail.setIsProActive(request.get().getIsProactive());
            reqDetail.setCreatedDate(CstDateTimeFormatter.parseDateTime(request.get().getCreated()));
            reqDetail.setStartDate(CstDateTimeFormatter.parseDateTime(request.get().getStartDate()));
            reqDetail.setFinishDate(CstDateTimeFormatter.parseDateTime(request.get().getFinishDate()));
            reqDetail.setEntryBy(request.get().getCreatedBy());
            reqDetail.setReasonCode1(reasonCodeService.getReasonCodeName(request.get().getCstReasonCode().getFirstReason()));
            reqDetail.setReasonCode2(reasonCodeService.getReasonCodeName(request.get().getCstReasonCode().getSecondReason()));
            reqDetail.setReasonCode3(reasonCodeService.getReasonCodeName(request.get().getCstReasonCode().getThirdReason()));
            reqDetail.setRootCauseResponsability(reasonCodeService.getReasonCodeName(request.get().getCstRootCause().getCauseResponsabilityId()));
            reqDetail.setRootCauseCategory(reasonCodeService.getReasonCodeName(request.get().getCstRootCause().getCauseCategoryId()));
            reqDetail.setRootCauseReason(reasonCodeService.getReasonCodeName(request.get().getCstRootCause().getCauseReasonId()));
            return Optional.of(reqDetail);
        }

        return Optional.ofNullable(null);
    }

    public List<CstRequestListDto> getCstRequestList(CstRequestFilterDto filterDto){

        List<CstRequest> cstRequestList = getCstRequestsByExample(filterDto);

        if(!cstRequestList.isEmpty()){
            return cstRequestList.stream().map(request -> {
                CstRequestListDto listDto = CstRequestMapper.mapToCstRequestListDto(request);
                listDto.setLcc(lccService.getLccShortName(request.getLccId()));
                listDto.setReasonCode(reasonCodeService.getReasonCodeName(request.getCstReasonCode().getFirstReason()));
                listDto.setType(getRequestType(request.getRequestTypeId()));
                return listDto;
            }).collect(Collectors.toList());
        }

        return Collections.EMPTY_LIST;
    }

    public String getRequestType(Long requestType){
       return cstRequestTypeRepository.findById(requestType).orElse(CstRequestType.builder().shortName("").build()).getShortName();
    }

    public CstRequestInitDto getInitLists() {
        List<ImapDto> lccs = new ArrayList<>();
        lccService.getLccs().stream().forEach(lcc -> lccs.add(ImapDto.builder().key(lcc.getName()).value(lcc.getId()).build()));

        List<ImapDto> reasonCodes = new ArrayList<>();
        reasonCodeService.reasonCodes().stream().forEach(rc -> reasonCodes.add(ImapDto.builder().key(rc.getName()).value(rc.getId()).build()));

        List<ImapDto> requestTypes = new ArrayList<>();
        cstRequestTypeRepository.findAll().stream().forEach(type -> requestTypes.add(ImapDto.builder().key(type.getShortName() + " " + type.getName()).value(type.getId()).build()));

        return CstRequestInitDto.builder().lccList(lccs).reasonCodeList(reasonCodes).requestTypeList(requestTypes).build();
    }
}
