/**
 * Created by adrian
 */
export class Overview {
    phrase: string;
    totalWorkouts: number;
    totalActivities: number;
    weight: number;
    height: number;
}

