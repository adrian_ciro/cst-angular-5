package com.kn.il.cst.service;

import com.kn.il.cst.model.datapool.domain.LogisticsControlCenter;
import com.kn.il.cst.model.datapool.repository.LogisticsControlCenterRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Getter
@Setter
public class LogisticsControlCenterService {

    private LogisticsControlCenterRepository logisticsControlCenterRepository;

    public LogisticsControlCenterService(LogisticsControlCenterRepository logisticsControlCenterRepository) {
        this.logisticsControlCenterRepository = logisticsControlCenterRepository;
    }

    public List<LogisticsControlCenter> getLccs(){
        return logisticsControlCenterRepository.findAll();
    }

    public String getLccShortName(Long lccId){
        return logisticsControlCenterRepository.findById(lccId).orElse(LogisticsControlCenter.builder().shortName("").build()).getShortName();
    }
}

