package com.kn.il.cst.model.datapool.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "LOGISTICS_CONTROL_CENTER")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogisticsControlCenter {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lcc_id_seq")
    @SequenceGenerator(name="lcc_id_seq", sequenceName="LCC_SEQ", allocationSize=1, initialValue=100)
    private Long id;

    @Column
    private Long branchOfficeId;

    @Column
    private String name;

    @Column
    private String shortName;

    @Column
    private String description;

    @Column
    private String requestedBy;

    @Column
    private Long countryId;

    @Column
    private String timeZoneCode;

    @Column
    @Type(type="yes_no")
    private Boolean isActive;


}
