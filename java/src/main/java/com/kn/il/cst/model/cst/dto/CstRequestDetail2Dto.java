package com.kn.il.cst.model.cst.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequestDetail2Dto {

    private Long id;

    private String requestCode;

    private String description;

    private Boolean isNew;

    private String requestType;

    private String priority;

    private String status;

    private String customer;

    private String customerSite;

    private String lcc;

    private String requestor;

    private String assignTo;

    private String entryBy;

    private String createdDate;

    private String startDate;

    private String finishDate;

    private String reasonCode1;

    private String reasonCode2;

    private String reasonCode3;

    private String rootCauseResponsability;

    private String rootCauseCategory;

    private String rootCauseReason;

    private Boolean hasEscalationEmail;

    private Boolean isProActive;

}
