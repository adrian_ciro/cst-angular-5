package com.kn.il.cst.model.cst.mapper;

import com.kn.il.cst.model.cst.domain.CstReasonCode;
import com.kn.il.cst.model.cst.domain.CstRequest;
import com.kn.il.cst.model.cst.domain.CstRootCause;
import com.kn.il.cst.model.cst.dto.CstRequestDetail2Dto;
import com.kn.il.cst.model.cst.dto.CstRequestDto;
import com.kn.il.cst.model.cst.dto.CstRequestFilterDto;
import com.kn.il.cst.model.cst.dto.CstRequestListDto;
import com.kn.il.cst.util.CstDateTimeFormatter;

public class CstRequestMapper {

    public static CstRequest mapToCstRequest(CstRequestDto dto){
        CstRequest cstRequest = CstRequest.builder().id(dto.getId()).description(dto.getDescription())
                .customerProfileCode(dto.getCustomerProfileCode())
                .requestTypeId(dto.getRequestTypeId()).lccId(dto.getLccId())
                .statusId(dto.getStatusId()).requestorId(dto.getRequestorId()).involvedPartyId(dto.getInvolvedPartyId())
                .isProactive(dto.getIsProactive()).priority(dto.getPriority()).build();

        return cstRequest;
    }

    public static CstRequest mapToCstRequest(CstRequestFilterDto dto){
        CstRequest cstRequest = CstRequest.builder().id(dto.getId()).requestCode(dto.getRequestCode())
                .customerProfileCode(dto.getCustomerProfileCode()).build();
        cstRequest.setCreated(null);
        cstRequest.setModified(null);
        return cstRequest;
    }

    public static CstReasonCode mapToCstReasonCode(CstRequestDto dto){
        CstReasonCode cstReasonCode = CstReasonCode.builder().id(dto.getId())
                .firstReason(dto.getReasonCode1()).secondReason(dto.getReasonCode2())
                .thirdReason(dto.getReasonCode3()).build();

        return cstReasonCode;
    }

    public static CstRootCause mapToRootCause(CstRequestDto dto){
        CstRootCause cstRootCause = CstRootCause.builder().id(dto.getId())
                .causeResponsabilityId(dto.getRootCauseResponsability())
                .causeCategoryId(dto.getRootCauseCategory())
                .causeReasonId(dto.getRootCauseReason()).build();

        return cstRootCause;
    }

    public static CstRequestListDto mapToCstRequestListDto(CstRequest request){
        return CstRequestListDto.builder().id(request.getId()).reqId(request.getRequestCode())
                .isNew(request.getStatusId()==1).priority(request.getPriority()).status(request.getStatusId()==1?"Created":"Other status")
                .customer(request.getCustomerProfileCode()+" / "+request.getCustomerProfileName())
                .creationDate(CstDateTimeFormatter.parseDateTime(request.getCreated()))
                .requestor("Manager")
                .hasAlert(false).hasLinkedReferences(true).hasDocuments(true).hasEscalationEmail(false).build();
    }

    public static CstRequestDetail2Dto mapToRequestDetail2Dto(CstRequest request){
        return CstRequestDetail2Dto.builder().id(request.getId()).requestType(request.getRequestCode())
                .isNew(request.getStatusId()==1).priority(request.getPriority()).status(request.getStatusId()==1?"Created":"Other status")
                .customer(request.getCustomerProfileCode()+" / "+request.getCustomerProfileName())
                .createdDate(CstDateTimeFormatter.parseDateTime(request.getCreated()))
                .description(request.getDescription())
                .isProActive(request.getIsProactive()).hasEscalationEmail(false).build();
    }
}
