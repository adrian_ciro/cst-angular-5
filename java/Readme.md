# CST BACKEND APPLICATION 

A simple spring boot application using Spring framework 5


## Versioning

|            | Versions |
|------------|----------|
| Java       | 8        |
| Springboot | 2.0.1    |
| Gradle     | 5.2      |       



## Building

To build with running unit tests

```shell
./gradlew clean build
```

To run the application locally

```shell
./gradlew bootRun
```