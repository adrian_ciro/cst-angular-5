package com.kn.il.cst.model.cst.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Base abstract class for entities which will hold definitions for created, last modified by and created,
 * last modified by date.
 */
@MappedSuperclass
//@Audited
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public abstract class AbstractAuditingEntity implements Serializable {


    @CreatedBy
    @Column(name = "CREATE_USER", nullable = false, updatable = false)
    private String createdBy;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @LastModifiedBy
    @Column(name = "UPDATE_USER")
    private String updatedBy;

    @LastModifiedDate
    @Column
    private LocalDateTime modified = LocalDateTime.now();


}
