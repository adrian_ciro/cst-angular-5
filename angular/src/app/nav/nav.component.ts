import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'nav-component',
    templateUrl: 'nav.component.html'
})
export class NavComponent {

    activeLink: string;
    userpic: string;

    constructor(public router: Router) {
        this.activeLink = router.url.substr(1);
        this.userpic = 'images/user.png';
    }

    isActive(current: string): boolean {
        return this.activeLink === current;
    }

    logout() {
        localStorage.removeItem('id_token');
        this.router.navigate(['login']);
    }

}
