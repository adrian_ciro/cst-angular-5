
export class RequestDataTable {
   id?: number;
   reqId?: string;
   isNew?: boolean;
   type?: string;
   priority?: string;
   status?: string;
   customer?: string;
   customerSite?: string;
   lcc?: string;
   requestor?: string;
   reasonCode?: string;
   creationDate?: string;
   hasLinkedReferences?: boolean;
   hasAlert?: boolean;
   hasDocuments?: boolean;
   hasEscalationEmail?: boolean
}

