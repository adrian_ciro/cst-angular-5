package com.kn.il.cst.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kn.il.cst.model.cst.domain.CstRequest;
import com.kn.il.cst.model.cst.dto.CstRequestDetail2Dto;
import com.kn.il.cst.model.cst.dto.CstRequestDetailDto;
import com.kn.il.cst.model.cst.dto.CstRequestDto;
import com.kn.il.cst.model.cst.dto.CstRequestFilterDto;
import com.kn.il.cst.model.cst.mapper.CstRequestMapper;
import com.kn.il.cst.service.CstRequestService;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@Log
public class CstRequestController {

    private CstRequestService cstRequestService;

    public CstRequestController(CstRequestService cstRequestService) {
        this.cstRequestService = cstRequestService;
    }

    @PutMapping(value = "/cstRequest")
    @Timed
    public ResponseEntity createCstRequest(@Valid @RequestBody CstRequestDto requestDto){
        cstRequestService.createNewCstRequest(requestDto);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/cstRequestList")
    @Timed
    public ResponseEntity getCstRequests(@Valid @RequestBody CstRequestFilterDto filterDto){
        return ResponseEntity.ok(cstRequestService.getCstRequestList(filterDto));
    }

    @PostMapping("/cstRequestDetail")
    @Timed
    public ResponseEntity getRequestDetail(@Valid @RequestBody CstRequestDetailDto detailDto){
        Optional<CstRequestDetail2Dto> reqDetail = cstRequestService.getRequestDetail(detailDto);
        return reqDetail.isPresent() ? ResponseEntity.ok(reqDetail.get()) : ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/requestInit")
    @Timed
    public ResponseEntity getInfoLists(@Valid @RequestBody CstRequestFilterDto filterDto){
        return ResponseEntity.ok(cstRequestService.getInitLists());
    }

}
