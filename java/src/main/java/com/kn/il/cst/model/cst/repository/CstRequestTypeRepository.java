package com.kn.il.cst.model.cst.repository;

import com.kn.il.cst.model.cst.domain.CstRequestType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CstRequestTypeRepository extends JpaRepository<CstRequestType, Long>{


}
