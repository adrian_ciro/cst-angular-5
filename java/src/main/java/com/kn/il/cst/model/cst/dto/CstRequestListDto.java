package com.kn.il.cst.model.cst.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequestListDto {

    private Long id;

    private String reqId;

    private Boolean isNew;

    private String type;

    private String priority;

    private String status;

    private String customer;

    private String customerSite;

    private String lcc;

    private String requestor;

    private String reasonCode;

    private String creationDate;

    private Boolean hasLinkedReferences;

    private Boolean hasAlert;

    private Boolean hasDocuments;

    private Boolean hasEscalationEmail;

}
