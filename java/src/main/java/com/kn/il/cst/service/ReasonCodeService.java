package com.kn.il.cst.service;

import com.kn.il.cst.model.datapool.domain.ReasonCode;
import com.kn.il.cst.model.datapool.repository.ReasonCodeRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@Getter
@Setter
public class ReasonCodeService {

    private ReasonCodeRepository reasonCodeRepository;

    public ReasonCodeService(ReasonCodeRepository reasonCodeRepository) {
        this.reasonCodeRepository = reasonCodeRepository;
    }

    public List<ReasonCode> reasonCodes(){
        return reasonCodeRepository.findAll();
    }

    public String getReasonCodeName(Long reasonCodeId) {
        if(StringUtils.isEmpty(reasonCodeId)){
            return "";
        }
        return reasonCodeRepository.findById(reasonCodeId).orElse(ReasonCode.builder().name("").build()).getName();
    }
}
