export class Api {
    public static API_ENDPOINT = 'http://localhost:8080/api/';
    public static API_ENDPOINT_DEV = 'http://127.0.0.1:8080/api/';

    public static API_LIST_REQUEST = Api.API_ENDPOINT + "cstRequestList";
    public static API_CREATE_INIT = Api.API_ENDPOINT + "requestInit";
    public static API_CREATE_REQUEST = Api.API_ENDPOINT + "cstRequest";
    public static API_DETAIL_REQUEST = Api.API_ENDPOINT + "cstRequestDetail";
}
