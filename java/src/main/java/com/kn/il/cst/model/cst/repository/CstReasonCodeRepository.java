package com.kn.il.cst.model.cst.repository;

import com.kn.il.cst.model.cst.domain.CstReasonCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CstReasonCodeRepository extends JpaRepository<CstReasonCode, Long>{


}
