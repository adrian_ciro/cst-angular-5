import {Component} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {RequestDetail} from "./request-details.model";
import {isNullOrUndefined} from "util";
import {contentHeaders} from "../common/headers";
import {Api} from "../common/api";

@Component({
    selector: 'request-details-component',
    templateUrl: 'request-details.component.html'
})
export class RequestDetailsComponent {

  public error_msg = '';
  public model: RequestDetail;

  constructor(public router: Router, public http: HttpClient, public route: ActivatedRoute) {
    let id;
    this.route.paramMap.subscribe((params : ParamMap) => id= params.get('id'));
    console.log(id);
    this.initialize(id);
  }

  initialize(id: number): void {
     this.model=new RequestDetail();

     if(!isNullOrUndefined(id)){
       let body = {customerProfileCode: "BEVIL01", id: id};
       let options = {headers: contentHeaders};

       this.http.post<RequestDetail>(Api.API_DETAIL_REQUEST, body, options)
         .subscribe(
           response => {
             this.model = response;
           },
           error => {
             this.error_msg = RequestDetailsComponent.name + error.message;
             console.log("Error in " + RequestDetailsComponent.name);
           }
         );
     }

  }

}
