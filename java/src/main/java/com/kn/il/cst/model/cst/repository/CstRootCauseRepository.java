package com.kn.il.cst.model.cst.repository;

import com.kn.il.cst.model.cst.domain.CstRootCause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CstRootCauseRepository extends JpaRepository<CstRootCause, Long>{


}
