package com.kn.il.cst.util;

import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CstDateTimeFormatter {

    private final static String DATE_PATTERN = "yyyy-MM-dd HH:mm";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);

    public static LocalDateTime getDateTime(String date){
        return StringUtils.isEmpty(date) ? null : LocalDateTime.parse(date, formatter);
    }

    public static String parseDateTime(LocalDateTime date){
        return StringUtils.isEmpty(date) ? "" : formatter.format(date);
    }

}
