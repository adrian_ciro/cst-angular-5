package com.kn.il.cst.model.cst.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "CST_CANCELED_REQUEST")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstCanceledRequest {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cst_canceled_request_id_seq")
    @SequenceGenerator(name="cst_canceled_request_id_seq", sequenceName="CANCELED_REQ_SEQ", allocationSize=1, initialValue=1)
    private Long id;

    @Column(name = "CANCELED_USER")
    private String canceledByUser;

    @Column(name = "CANCELED_REASON")
    private String canceledReason;

    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="REQUEST_ID")
    private CstRequest cstRequest;
}
