package com.kn.il.cst.model.cst.domain;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "CST_REQUEST")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequest extends AbstractAuditingEntity{

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="request_id_seq")
    @SequenceGenerator(name="request_id_seq", sequenceName="REQ_SEQ", allocationSize=1, initialValue=100)
    private Long id;

    @NotNull
    @Column(name = "REQUEST_CODE")
    private String requestCode;

    @Column
    private String description;

    @Column(name = "CUSTOMER_PROFILE_ID")
    private Long customerProfileId;

    @Column(name = "CUSTOMER_PROFILE_CODE")
    private String customerProfileCode;

    @Column(name = "CUSTOMER_PROFILE_NAME")
    private String customerProfileName;

    @NotNull
    @Column(name = "REQUEST_TYPE_ID")
    private Long requestTypeId;

    @NotNull
    @Column(name = "LCC_ID")
    private Long lccId;

    @Column(name = "STATUS_ID")
    private Long statusId;

    @OneToOne(fetch=FetchType.LAZY, mappedBy="cstRequest")
    private CstReasonCode cstReasonCode;

    @OneToOne(fetch=FetchType.LAZY, mappedBy="cstRequest")
    private CstRootCause cstRootCause;

    @OneToOne(fetch=FetchType.LAZY, mappedBy="cstRequest")
    private CstCanceledRequest cstCanceledRequest;

    @Column(name = "REQUESTOR_ID")
    private Long requestorId;

    @Column(name = "INVOLVED_PARTY_ID")
    private Long involvedPartyId;

    @Column(name = "CANCELED_ID")
    private Long canceledId;

    @Column(name = "START_DT")
    private LocalDateTime startDate;

    @Column(name = "FINISH_DT")
    private LocalDateTime finishDate;

    @Column(name = "IS_ESCALATION_EMAIL_SENT")
    @Type(type="yes_no")
    private Boolean isEscalationEmailSent;

    @Column(name = "IS_PROACTIVE")
    @Type(type="yes_no")
    private Boolean isProactive;

    @NotNull
    @Column
    private String priority;

    @Column
    private String remark;

    @Column(name = "IS_ACTIVE")
    @Type(type="yes_no")
    private Boolean isActive;

    @Column
    private Long trace;

}
