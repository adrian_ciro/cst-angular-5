package com.kn.il.cst.model.cst.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "CST_REASON_CODE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstReasonCode {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cst_reason_code_id_seq")
    @SequenceGenerator(name="cst_reason_code_id_seq", sequenceName="CST_REASON_CODE_SEQ", allocationSize=1, initialValue=100)
    private Long id;

    @Column(name = "FIRST_REASON_CODE_ID")
    private Long firstReason;

    @Column(name = "SECOND_REASON_CODE_ID")
    private Long secondReason;

    @Column(name = "THIRD_REASON_CODE_ID")
    private Long thirdReason;

    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="REQUEST_ID")
    private CstRequest cstRequest;
}
