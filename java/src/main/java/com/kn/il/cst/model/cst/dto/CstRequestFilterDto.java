package com.kn.il.cst.model.cst.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequestFilterDto {

    private Long id;

    private String requestCode;

    private String customerProfileCode;

}
