
insert into REASON_CODE (REASON_CODE_ID, REASON_CODE, NAME, REASON_CODE_TYPE, DESCRIPTION, REQUESTED_BY, RC_PARENT_ID, CST_CODE)
values (1, '001', 'Damaged material', 'CST', 'Damaged Material', 'Company INC', null, '001');
insert into REASON_CODE (REASON_CODE_ID, REASON_CODE, NAME, REASON_CODE_TYPE, DESCRIPTION, REQUESTED_BY, RC_PARENT_ID, CST_CODE)
values (2, '002', 'Material lost', 'CST', 'Goods has been lost during transit', 'Company INC', null, '002');
insert into REASON_CODE (REASON_CODE_ID, REASON_CODE, NAME, REASON_CODE_TYPE, DESCRIPTION, REQUESTED_BY, RC_PARENT_ID, CST_CODE)
values (3, '003', 'Incorrect material delivered', 'CST', 'Incorrect material delivered', 'Company INC', null, '003');
insert into REASON_CODE (REASON_CODE_ID, REASON_CODE, NAME, REASON_CODE_TYPE, DESCRIPTION, REQUESTED_BY, RC_PARENT_ID, CST_CODE)
values (4, '004', 'Documents missing or incomplete', 'CST', 'Documents missing or incomplete', 'Company INC', null, '004');
insert into REASON_CODE (REASON_CODE_ID, REASON_CODE, NAME, REASON_CODE_TYPE, DESCRIPTION, REQUESTED_BY, RC_PARENT_ID, CST_CODE)
values (5, '005', 'Incorrect customs clearance', 'CST', 'Incorrect customs clearance', 'Company INC', null, '005');

insert into LOGISTICS_CONTROL_CENTER (ID, BRANCH_OFFICE_ID, NAME, SHORT_NAME, DESCRIPTION, REQUESTED_BY, TIME_ZONE_CODE, COUNTRY_ID)
values (1, 1, 'KN Luxembourg Roche', 'KN LUX Roche', 'LCC in KN LUX Roche', 'User 1', 'Europe/Berlin', 1);
insert into LOGISTICS_CONTROL_CENTER (ID, BRANCH_OFFICE_ID, NAME, SHORT_NAME, DESCRIPTION, REQUESTED_BY, TIME_ZONE_CODE, COUNTRY_ID)
values (2, 1, 'LCC Vancouver', 'KN_VAN', 'LCC in Vancouver', 'User 2', 'America/Vancouver', 1);
insert into LOGISTICS_CONTROL_CENTER (ID, BRANCH_OFFICE_ID, NAME, SHORT_NAME, DESCRIPTION, REQUESTED_BY, TIME_ZONE_CODE, COUNTRY_ID)
values (3, 1, 'LCC Stuttgart-Degerloch', 'KN_Stgt', 'LCC in Stuttgart Degerloch', 'User 3', 'Europe/Berlin', 1);


insert into CST_REQUEST_TYPE (ID, SHORT_NAME, NAME, DESCRIPTION, CUSTOMER_PROFILE_CODE)
values (1,'GLR', 'Good Luck Request', 'Request that give good luck to the customer','CHBSL19');
insert into CST_REQUEST_TYPE (ID, SHORT_NAME, NAME, DESCRIPTION, CUSTOMER_PROFILE_CODE)
values (2,'NCR', 'Non Conformity Report', 'Request that give good luck to the customer','BEVIL01');
insert into CST_REQUEST_TYPE (ID, SHORT_NAME, NAME, DESCRIPTION, CUSTOMER_PROFILE_CODE)
values (3,'DOC', 'Document Request', 'This request is started if a participating party requests a certain document','BEVIL01');
insert into CST_REQUEST_TYPE (ID, SHORT_NAME, NAME, DESCRIPTION, CUSTOMER_PROFILE_CODE)
values (4,'REQ', 'General Request', 'A not further described request, which can be used for miscellaneous issues','BEVIL01');
insert into CST_REQUEST_TYPE (ID, SHORT_NAME, NAME, DESCRIPTION, CUSTOMER_PROFILE_CODE)
values (5,'DOC', 'Document Request', 'This request is started if a participating party requests a certain document','CHBSL19');
insert into CST_REQUEST_TYPE (ID, SHORT_NAME, NAME, DESCRIPTION, CUSTOMER_PROFILE_CODE)
values (6,'REQ', 'General Request', 'A not further described request, which can be used for miscellaneous issues','CHBSL19');


insert into CST_REQUEST (ID, REQUEST_CODE, REQUEST_TYPE_ID, DESCRIPTION, CUSTOMER_PROFILE_ID, CUSTOMER_PROFILE_CODE, CUSTOMER_PROFILE_NAME, LCC_ID, STATUS_ID, REQUESTOR_ID, INVOLVED_PARTY_ID,
START_DT, FINISH_DT,IS_ESCALATION_EMAIL_SENT, IS_PROACTIVE, PRIORITY, CREATED, CREATE_USER, UPDATE_USER, MODIFIED, TRACE)
values (1, 'NCR-20171110-1', 2, 'Description Test for NCR', '1', 'BEVIL01', 'Caterpillar', 1, 1, 1, 1,
to_date('01-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), to_date('10-12-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 'N', 'N', 'HI', to_date('10-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'),
'Admin', 'User1', to_date('10-12-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 3);

insert into CST_REASON_CODE (ID, REQUEST_ID, FIRST_REASON_CODE_ID, SECOND_REASON_CODE_ID, THIRD_REASON_CODE_ID)
values (1, 1, 1, 2, null);
insert into CST_ROOT_CAUSE (ID, REQUEST_ID, CAUSE_RESPONSABILITY_ID, CAUSE_CATEGORY_ID, CAUSE_REASON_ID)
values (1, 1, 5, null, null);

insert into CST_REQUEST (ID, REQUEST_CODE, REQUEST_TYPE_ID, DESCRIPTION, CUSTOMER_PROFILE_ID, CUSTOMER_PROFILE_CODE, CUSTOMER_PROFILE_NAME, LCC_ID, STATUS_ID, REQUESTOR_ID, INVOLVED_PARTY_ID,
START_DT, FINISH_DT,IS_ESCALATION_EMAIL_SENT, IS_PROACTIVE, PRIORITY, CREATED, CREATE_USER, UPDATE_USER, MODIFIED, TRACE)
values (2, 'DOC-20171110-1', 3, 'Description Test for DOC', '1', 'BEVIL01', 'Caterpillar', 2, 1, 1, 1,
to_date('10-11-2017 11:30:00', 'dd-mm-yyyy hh24:mi:ss'), to_date('12-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 'N', 'N', 'HI', to_date('10-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'),
'Admin', 'User1', to_date('12-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 3);

insert into CST_REASON_CODE (ID, REQUEST_ID, FIRST_REASON_CODE_ID, SECOND_REASON_CODE_ID, THIRD_REASON_CODE_ID)
values (2, 2, 1, 2, null);
insert into CST_ROOT_CAUSE (ID, REQUEST_ID, CAUSE_RESPONSABILITY_ID, CAUSE_CATEGORY_ID, CAUSE_REASON_ID)
values (2, 2, 5, null, null);

insert into CST_REQUEST (ID, REQUEST_CODE, REQUEST_TYPE_ID, DESCRIPTION, CUSTOMER_PROFILE_ID, CUSTOMER_PROFILE_CODE, CUSTOMER_PROFILE_NAME, LCC_ID, STATUS_ID, REQUESTOR_ID, INVOLVED_PARTY_ID,
START_DT, FINISH_DT,IS_ESCALATION_EMAIL_SENT, IS_PROACTIVE, PRIORITY, CREATED, CREATE_USER, UPDATE_USER, MODIFIED, TRACE)
values (3, 'REQ-20171115-1', 4, 'Description Test for REQ', '1', 'BEVIL01', 'Caterpillar', 3, 1, 1, 1,
to_date('15-11-2017 11:30:00', 'dd-mm-yyyy hh24:mi:ss'), null, 'N', 'N', 'MED', to_date('15-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'),
'Admin', 'User1', to_date('12-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 3);

insert into CST_REASON_CODE (ID, REQUEST_ID, FIRST_REASON_CODE_ID, SECOND_REASON_CODE_ID, THIRD_REASON_CODE_ID)
values (3, 3, 5, null, null);
insert into CST_ROOT_CAUSE (ID, REQUEST_ID, CAUSE_RESPONSABILITY_ID, CAUSE_CATEGORY_ID, CAUSE_REASON_ID)
values (3, 3, null, null, null);

insert into CST_REQUEST (ID, REQUEST_CODE, REQUEST_TYPE_ID, DESCRIPTION, CUSTOMER_PROFILE_ID, CUSTOMER_PROFILE_CODE, CUSTOMER_PROFILE_NAME, LCC_ID, STATUS_ID, REQUESTOR_ID, INVOLVED_PARTY_ID,
START_DT, FINISH_DT,IS_ESCALATION_EMAIL_SENT, IS_PROACTIVE, PRIORITY, CREATED, CREATE_USER, UPDATE_USER, MODIFIED, TRACE)
values (4, 'REQ-20171115-1', 4, 'Description Test for REQ', '1', 'BEVIL01', 'Caterpillar', 1, 2, 1, 1,
to_date('15-11-2017 11:30:00', 'dd-mm-yyyy hh24:mi:ss'), null, 'N', 'N', 'MED', to_date('15-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'),
'Admin', 'User1', to_date('12-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 3);

insert into CST_REASON_CODE (ID, REQUEST_ID, FIRST_REASON_CODE_ID, SECOND_REASON_CODE_ID, THIRD_REASON_CODE_ID)
values (4, 4, 2, null, null);
insert into CST_ROOT_CAUSE (ID, REQUEST_ID, CAUSE_RESPONSABILITY_ID, CAUSE_CATEGORY_ID, CAUSE_REASON_ID)
values (4, 4, null, null, null);

insert into CST_REQUEST (ID, REQUEST_CODE, REQUEST_TYPE_ID, DESCRIPTION, CUSTOMER_PROFILE_ID, CUSTOMER_PROFILE_CODE, CUSTOMER_PROFILE_NAME, LCC_ID, STATUS_ID, REQUESTOR_ID, INVOLVED_PARTY_ID,
START_DT, FINISH_DT,IS_ESCALATION_EMAIL_SENT, IS_PROACTIVE, PRIORITY, CREATED, CREATE_USER, UPDATE_USER, MODIFIED, TRACE)
values (5, 'NCR-20171115-2', 2, 'Description Test for REQ', '1', 'BEVIL01', 'Caterpillar', 2, 2, 1, 1,
to_date('15-11-2017 11:30:00', 'dd-mm-yyyy hh24:mi:ss'), null, 'N', 'N', 'HI', to_date('15-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'),
'Admin', 'User1', to_date('12-11-2017 07:30:00', 'dd-mm-yyyy hh24:mi:ss'), 3);

insert into CST_REASON_CODE (ID, REQUEST_ID, FIRST_REASON_CODE_ID, SECOND_REASON_CODE_ID, THIRD_REASON_CODE_ID)
values (5, 5, 3, null, null);
insert into CST_ROOT_CAUSE (ID, REQUEST_ID, CAUSE_RESPONSABILITY_ID, CAUSE_CATEGORY_ID, CAUSE_REASON_ID)
values (5, 5, 4, null, null);