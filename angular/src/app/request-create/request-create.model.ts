import {Imap} from "../common/Imap.model";

export class RequestCreate {

     requestTypeId?: number;
     customerProfileCode?: string;
     description?: string;
     lccId?: number;
     customerSite?: string;
     reasonCode1?: string;
     reasonCode2?: string;
     reasonCode3?: string;
     rootCauseResponsability?: string;
     rootCauseCategory?: string;
     rootCauseReason?: string;
     priority?: string;
     requestorId?: number;
     involvedPartyId?: number;
     customerReqId?: number;
     assignedTo?: string;
     isProactive?: boolean;
}


export class creationLists {
   requestTypeList: Array<Imap>;
   lccList: Array<Imap>;
   reasonCodeList: Array<Imap>;
}
