package com.kn.il.cst.model.cst.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequestDto {


    private Long id;

    private String requestCode;

    private String description;

    private String assignedTo;

    @NotNull
    private String customerProfileCode;

    private String customerSite;

    @NotNull
    private Long requestTypeId;

    @NotNull
    private Long lccId;

    private Long statusId;

    private Long reasonCode1;

    private Long reasonCode2;

    private Long reasonCode3;

    private Long rootCauseResponsability;

    private Long rootCauseCategory;

    private Long rootCauseReason;

    private Long requestorId;

    private Long involvedPartyId;

    private Boolean isEscalationEmailSent;

    private Boolean isValid;

    private Boolean isProactive;

    private String priority;

}
