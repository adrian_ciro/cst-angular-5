import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RequestDataTable} from "./request-list.model";
import {HttpClient} from "@angular/common/http";
import {contentHeaders} from "../common/headers";
import {Api} from "../common/api";

declare var $: any;

@Component({
    selector: 'request-list-component',
    templateUrl: 'request-list.component.html'
})
export class RequestListComponent implements OnInit{

  public error_msg = '';
  public model: Array<RequestDataTable>;

  constructor(public router: Router, public http: HttpClient) {
    this.initialize();
  }

  initialize(): void {
    this.model= [];
    let body = {customerProfileCode: "BEVIL01"};
    let options = {headers: contentHeaders};

    this.http.post<Array<RequestDataTable>>(Api.API_LIST_REQUEST, body, options)
      .subscribe(
        response => {
          this.model = response;

        },
        error => {
          this.error_msg = RequestListComponent.name + error.message;
          console.log("Error in " + RequestListComponent.name);
        }
      );
  }

  ngOnInit(): void {
    //Toggle component
    $('.btn-toggle').click(function() {
      $(this).find('.btn').toggleClass('active');

      if ($(this).find('.btn-primary').length>0) {
        $(this).find('.btn').toggleClass('btn-primary');
      }
      if ($(this).find('.btn-danger').length>0) {
        $(this).find('.btn').toggleClass('btn-danger');
      }
      if ($(this).find('.btn-success').length>0) {
        $(this).find('.btn').toggleClass('btn-success');
      }
      if ($(this).find('.btn-info').length>0) {
        $(this).find('.btn').toggleClass('btn-info');
      }
      $(this).find('.btn').toggleClass('btn-default');
    });

    //DateTime pickers
    // $('.datetimepicker').datetimepicker();
  }

}
