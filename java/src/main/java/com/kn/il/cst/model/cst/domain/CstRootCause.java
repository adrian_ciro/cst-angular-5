package com.kn.il.cst.model.cst.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "CST_ROOT_CAUSE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRootCause {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cst_root_cause_id_seq")
    @SequenceGenerator(name="cst_root_cause_id_seq", sequenceName="CST_ROOT_CAUSE_SEQ", allocationSize=1, initialValue=100)
    private Long id;

    @Column(name = "CAUSE_RESPONSABILITY_ID")
    private Long causeResponsabilityId;

    @Column(name = "CAUSE_CATEGORY_ID")
    private Long causeCategoryId;

    @Column(name = "CAUSE_REASON_ID")
    private Long causeReasonId;

    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="REQUEST_ID")
    private CstRequest cstRequest;
}
