export class RequestDetail {

  id: number;
  requestCode: string;
  description?: string;
  requestType?: string;
  lcc?: string;
  customerSite?: string;
  reasonCode1?: string;
  reasonCode2?: string;
  reasonCode3?: string;
  rootCauseResponsability?: string;
  rootCauseCategory?: string;
  rootCauseReason?: string;
  priority?: string;
  requestor?: string;
  involvedParty?: string;
  customerReq?: string;
  assignedTo?: string;
  status?: string;
  customer?: string;
  hasEscalationEmail?: string;
  isProActive?: string;
  createdDate?: string;
  startDate?: string;
  finishDate?: string;
  entryBy?: string;

}
