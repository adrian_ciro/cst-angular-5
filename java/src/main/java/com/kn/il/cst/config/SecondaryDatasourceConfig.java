package com.kn.il.cst.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackages = "com.kn.il.cst.model.datapool",
                       entityManagerFactoryRef = "datapoolEntityManagerFactory",
                       transactionManagerRef = "datapoolTransactionManager")
public class SecondaryDatasourceConfig{

    @Bean
    @ConfigurationProperties(prefix = "datasource.datapool")
    public DataSource datapoolDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean datapoolEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(datapoolDataSource())
                .packages("com.kn.il.cst.model.datapool")
                .persistenceUnit("datapool")
                .build();
    }

    @Bean
    public PlatformTransactionManager datapoolTransactionManager(@Qualifier("datapoolEntityManagerFactory") EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

}
