package com.kn.il.cst.model.datapool.repository;

import com.kn.il.cst.model.datapool.domain.ReasonCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReasonCodeRepository extends JpaRepository<ReasonCode, Long>{


}
