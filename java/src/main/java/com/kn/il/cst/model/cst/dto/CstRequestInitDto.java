package com.kn.il.cst.model.cst.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequestInitDto {

    private List<ImapDto> requestTypeList;

    private List<ImapDto> lccList;

    private List<ImapDto> reasonCodeList;

}
