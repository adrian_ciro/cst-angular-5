# CST FRONTEND APPLICATION 

Project based on Angular 5 using angular-cli as a builder.


## Versioning

|           | Versions |
|-----------|----------|
| Node      | 8.17.0   |
| Angular   | 5.2.11   |
| bootstrap | 4        |       



## Build

From angular folder, install the npm packages 

```shell
npm i
```

Most likely you need to start the dependencies manually

Run npm to start the project. 

```shell
npm start 
```

The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

---------------------------------------------------------

## NOTE

If `ng serve` does not work it is because you don't have angular cli, use `npm start` command.
You should install angular cli globally by following command.
`npm install -g @angular/cli@latest`

