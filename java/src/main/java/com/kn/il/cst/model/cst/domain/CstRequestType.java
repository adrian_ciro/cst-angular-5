package com.kn.il.cst.model.cst.domain;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CST_REQUEST_TYPE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CstRequestType {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="request_type_id_seq")
    @SequenceGenerator(name="request_type_id_seq", sequenceName="REQ_TYPE_SEQ", allocationSize=1, initialValue=100)
    private Long id;

    @Column
    private String name;

    @Column
    private String shortName;

    @Column
    private String description;

    @Column(name = "CUSTOMER_PROFILE_CODE")
    private String customerProfileCode;

    @Column(name = "IS_ACTIVE")
    @Type(type="yes_no")
    private Boolean isActive;
}
