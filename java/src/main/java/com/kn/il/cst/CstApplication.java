package com.kn.il.cst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class CstApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(CstApplication.class, args);
		System.out.println(context.getBeanDefinitionNames().length);
	}
}
