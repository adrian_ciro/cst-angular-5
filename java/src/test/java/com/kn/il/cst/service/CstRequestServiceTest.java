package com.kn.il.cst.service;

import com.kn.il.cst.AbstractJpaTest;
import com.kn.il.cst.model.cst.domain.CstReasonCode;
import com.kn.il.cst.model.cst.domain.CstRequest;
import com.kn.il.cst.model.cst.domain.CstRootCause;
import com.kn.il.cst.model.cst.dto.CstRequestDto;
import com.kn.il.cst.model.cst.dto.CstRequestFilterDto;
import com.kn.il.cst.util.CstDateTimeFormatter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import java.util.List;

import static org.junit.Assert.*;

@Import({CstRequestService.class, LogisticsControlCenterService.class, ReasonCodeService.class})
public class CstRequestServiceTest extends AbstractJpaTest{

    public static final String BEVIL01 = "BEVIL01";
    public static final String GBDXY55 = "GBDXY55";
    public static final String CREATED_BY = "creator";
    public static final String UPDATED_DBY = "updater";

    @Autowired
    CstRequestService cstRequestService;

    @Test
    public void createNewCstRequest() {
        CstRequestDto reqDto = CstRequestDto.builder().id(1L).requestCode("req1").customerProfileCode(BEVIL01)
                .requestTypeId(1L).lccId(1L).priority("HI").isValid(true).description("Description")
                .reasonCode1(1L).reasonCode2(2L).rootCauseCategory(1L).rootCauseResponsability(2L)
                .build();

        cstRequestService.createNewCstRequest(reqDto);

        List<CstRequest> cstRequests = cstRequestService.getCstRequestRepository().findAll();
        List<CstReasonCode> cstReasonCodes = cstRequestService.getCstReasonCodeRepository().findAll();
        List<CstRootCause> cstRootCauses = cstRequestService.getCstRootCauseRepository().findAll();

        assertEquals(1, cstRequests.size());
        assertEquals(1, cstReasonCodes.size());
        assertEquals(1, cstRootCauses.size());
    }

    @Test
    public void getCstRequestsByExample() {


        CstRequest req1 = CstRequest.builder().id(1L).requestCode("req1").customerProfileId(1L).customerProfileCode(BEVIL01)
                .requestTypeId(1L).lccId(1L).priority("HI").isProactive(true).description("Description")
                .startDate(CstDateTimeFormatter.getDateTime("2017-12-12 14:56"))
                .build();
        req1.setCreatedBy(CREATED_BY);
        req1.setUpdatedBy(UPDATED_DBY);
        cstRequestService.getCstRequestRepository().save(req1);

        CstRequest req2 = CstRequest.builder().id(2L).requestCode("req2").customerProfileId(1L).customerProfileCode(BEVIL01)
                .requestTypeId(1L).lccId(1L).priority("HI").isProactive(true).description("Description")
                .startDate(CstDateTimeFormatter.getDateTime("2017-11-11 14:56"))
                .build();
        req2.setCreatedBy(CREATED_BY);
        req2.setUpdatedBy(UPDATED_DBY);
        cstRequestService.getCstRequestRepository().save(req2);

        CstRequest req3 = CstRequest.builder().id(3L).requestCode("req3").customerProfileId(1L).customerProfileCode(GBDXY55)
                .requestTypeId(1L).lccId(1L).priority("HI").isProactive(true).description("Description")
                .startDate(CstDateTimeFormatter.getDateTime("2016-11-11 14:56"))
                .build();
        req3.setCreatedBy(CREATED_BY);
        req3.setUpdatedBy(UPDATED_DBY);
        cstRequestService.getCstRequestRepository().save(req3);


        CstRequestFilterDto filterDto = CstRequestFilterDto.builder().customerProfileCode(BEVIL01).build();
        List<CstRequest> cstRequests = cstRequestService.getCstRequestsByExample(filterDto);

        assertEquals(2, cstRequests.size());
    }


}