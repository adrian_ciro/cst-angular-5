import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Api} from '../common/api';
import {Overview} from './home.model';
import {contentHeaders} from '../common/headers';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})
export class HomeComponent {

  private msgPrefix = 'overview.';
  private error = '';
  private model: Overview;

  constructor(public router: Router, public http: HttpClient) {
    this.initialize();
  }

  initialize() {
    this.model = new Overview();
    let body = {};
    let headers = new HttpHeaders();

  }

}
