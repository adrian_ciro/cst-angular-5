package com.kn.il.cst.model.cst.repository;

import com.kn.il.cst.model.cst.domain.CstCanceledRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CstCanceledRequestRepository extends JpaRepository<CstCanceledRequest, Long>{


}
